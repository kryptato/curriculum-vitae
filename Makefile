.PHONY: clean help
.DEFAULT_GOAL := help

clean: ## Clean al the useless files (blg, xmpi, run.xml, bbl, bcf, log, out, aux, fls, gz, fdb_latexmk)
	find . -type f -regextype posix-extended -regex '.*\.(blg|xmpi|run.xml|bbl|bcf|log|out|aux|fls|gz|fdb_latexmk)' -delete

help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
